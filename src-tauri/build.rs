use dbus_codegen;
fn main() {
    let code = dbus_codegen::generate(
        &std::fs::read_to_string("./org.freedesktop.systemd1.xml").expect("reading xml"),
        &dbus_codegen::GenOpts {
            methodtype: None,
            ..Default::default()
        },
    ).expect("generating code");
    let out_dir = std::env::var("OUT_DIR").expect("OUT_DIR");
    std::fs::write(format!("{out_dir}/systemd_gen.rs"), code).expect("writing generated code");
    tauri_build::build()
}
