pub enum Terminal {
    GnomeTerminal,
    Kitty,
    WezTerm,
}

pub fn get_run_command(term: Terminal, args: &[&str]) -> (String, Vec<String>) {
    match term {
        Terminal::GnomeTerminal => ("gnome-terminal".to_string(), {
            let mut v = vec!["--".to_string()];
            v.append(&mut args.into_iter().map(|s| s.to_string()).collect::<Vec<_>>());
            v
        }),

        Terminal::Kitty => ("kitty".to_string(), {
            let mut v = vec!["-1".to_string()];
            v.append(&mut args.into_iter().map(|s| s.to_string()).collect::<Vec<_>>());
            v
        }),

        Terminal::WezTerm => ("wezterm".to_string(), {
            let mut v = vec!["start".to_string()];
            v.append(&mut args.into_iter().map(|s| s.to_string()).collect::<Vec<_>>());
            v
        })
    }
}
