#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
use anyhow::{anyhow, Result};
use dbus::blocking::Connection;
use std::collections::HashMap;

mod systemd_gen;
mod systemd;
mod terminal;

fn dbus_connection_from_session_specifier(session_type: &str) -> Result<Connection> {
    match session_type {
        "system" => Connection::new_system().map_err(|e| anyhow!(e)),
        "session" => Connection::new_session().map_err(|e| anyhow!(e)),
        _ => Err(anyhow!("invalid session specifier {session_type}")),
    }
}

fn make_json_err(e: anyhow::Error) -> String {
    let mut map = HashMap::new();
    map.insert("error", e.to_string());
    serde_json::to_string(&map).unwrap()
}
macro_rules! json_try {
    ($e:expr) => {
        match $e {
            Ok(v) => v,
            Err(e) => {
                eprintln!("json_try: got error: {e}");
                return make_json_err(anyhow!(e));
            }
        }
    };
}

#[tauri::command]
fn systemd_list_units(session_type: &str) -> String {
    //{{{
    let conn = json_try!(dbus_connection_from_session_specifier(session_type));
    let units = json_try!(systemd::list_units(&conn));
    serde_json::to_string(&units).unwrap()
} //}}}

#[tauri::command]
fn systemd_list_unit_files(session_type: &str) -> String {
    //{{{
    let conn = json_try!(dbus_connection_from_session_specifier(session_type));
    let units = json_try!(systemd::list_unit_files(&conn));
    serde_json::to_string(&units).unwrap()
} //}}}

#[tauri::command]
fn systemd_start_unit(unit: &str, mode: &str, session_type: &str) -> String {
    //{{{
    let conn = json_try!(dbus_connection_from_session_specifier(session_type));
    let job = json_try!(systemd::start_unit(&conn, unit, mode));
    serde_json::to_string(&job).unwrap()
} //}}}

#[tauri::command]
fn systemd_stop_unit(unit: &str, mode: &str, session_type: &str) -> String {
    //{{{
    let conn = json_try!(dbus_connection_from_session_specifier(session_type));
    let job = json_try!(systemd::stop_unit(&conn, unit, mode));
    serde_json::to_string(&job).unwrap()
} //}}}

#[tauri::command]
fn systemd_open_unit_logs(terminal: &str, unit: &str, session_type: &str) {
    //{{{
    use terminal::Terminal;
    let terminal = match terminal {
        "gnome-terminal" => Terminal::GnomeTerminal,
        "kitty" => Terminal::Kitty,
        "wezterm" => Terminal::WezTerm,
        _ => todo!("unknown terminal: {terminal}"),
    };
    use std::process::Command;
    let unit = unit.to_string();
    let journalctl_session_type = match session_type {
        "system" => "system",
        "session" => "user",
        _ => {
            eprintln!("invalid session_type specifier: {session_type}");
            return;
        }
    };
    std::thread::spawn(move || {
        let (cmd, args) = terminal::get_run_command(
            terminal,
            &[
                "journalctl",
                &format!("--{journalctl_session_type}"),
                "-f",
                "-u",
                &unit,
            ],
        );

        let result = Command::new(&cmd).args(&args).spawn();
        let mut child = match result {
            Ok(c) => c,
            Err(e) => {
                eprintln!("{}", e);
                return;
            }
        };

        let code = match child.wait() {
            Ok(code) => code,
            Err(e) => {
                eprintln!("{}", e);
                return;
            }
        };

        if !code.success() {
            eprintln!("non-zero exit code: {code}");
            return;
        }
    });
} //}}}

#[tauri::command]
fn systemd_get_unit_file_state(unit: &str, session_type: &str) -> String {
    let conn = json_try!(dbus_connection_from_session_specifier(session_type));
    json_try!(systemd::get_unit_file_state(&conn, unit))
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            systemd_list_units,
            systemd_list_unit_files,
            systemd_start_unit,
            systemd_stop_unit,
            systemd_open_unit_logs,
            systemd_get_unit_file_state,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
