use anyhow::{anyhow, Context, Result};
use dbus::blocking::Connection;
use serde::Serialize;
use std::time::Duration;

use crate::systemd_gen::OrgFreedesktopSystemd1Manager;

fn systemd_proxy<'p, 'c: 'p>(conn: &'c Connection, timeout: Option<u64>) -> dbus::blocking::Proxy<'p, &'p Connection> {
    conn.with_proxy(
        "org.freedesktop.systemd1",
        "/org/freedesktop/systemd1",
        Duration::from_millis(timeout.unwrap_or(1000)),
    )
}

#[derive(Debug, Serialize)]
pub struct ListUnitsResponse {
    units: Vec<ListUnitsResponseUnit>,
}

#[derive(Debug, Serialize)]
pub struct ListUnitsResponseUnit {
    name: String,
    description: String,
    load_state: String,
    active_state: String,
    sub_state: String,
    follow_unit: String,
    object_path: String,
    job_id: u32,
    job_type: String,
    job_path: String,
}

pub fn list_units(conn: &Connection) -> Result<ListUnitsResponse> {
    //{{{
    let units = systemd_proxy(conn, Some(10_000))
        .list_units()
        .context("calling ListUnits")?;
    let units = units
        .into_iter()
        .map(
            |(
                name,
                description,
                load_state,
                active_state,
                sub_state,
                follow_unit,
                object_path,
                job_id,
                job_type,
                job_path,
            )| ListUnitsResponseUnit {
                name,
                description,
                load_state,
                active_state,
                sub_state,
                follow_unit,
                object_path: object_path.to_string(),
                job_id,
                job_type,
                job_path: job_path.to_string(),
            },
        )
        .collect::<Vec<_>>();
    Ok(ListUnitsResponse { units })
} //}}}

pub fn list_unit_files(conn: &Connection) -> Result<Vec<(String, String)>> {
    //{{{
    let units = systemd_proxy(conn, Some(10_000))
        .list_unit_files()
        .context("calling ListUnitFiles")?;
    Ok(units)
} //}}}

pub fn start_unit(conn: &Connection, unit: &str, mode: &str) -> Result<String> {
    //{{{
    let allowed_modes = &[
        "replace",
        "fail",
        "isolate",
        "ignore-dependencies",
        "ignore-requirements",
    ];
    if !allowed_modes.contains(&mode) {
        return Err(anyhow!(
            "illegal mode {mode}: expected one of {}",
            allowed_modes.join(", ")
        ));
    }
    let job = systemd_proxy(conn, Some(60_000))
        .start_unit(unit, mode)
        .context("calling StartUnit")?;
    Ok(job.to_string())
} //}}}

pub fn stop_unit(conn: &Connection, unit: &str, mode: &str) -> Result<String> {
    //{{{
    let allowed_modes = &[
        "replace",
        "fail",
        "ignore-dependencies",
        "ignore-requirements",
    ];
    if !allowed_modes.contains(&mode) {
        return Err(anyhow!(
            "illegal mode {mode}: expected one of {}",
            allowed_modes.join(", ")
        ));
    }
    let job = systemd_proxy(conn, Some(60_000))
        .stop_unit(unit, mode)
        .context("calling StopUnit")?;
    Ok(job.to_string())
} //}}}

pub fn get_unit_file_state(conn: &Connection, unit: &str) -> Result<String> {
    //{{{
    let state = systemd_proxy(conn, Some(10_000))
        .get_unit_file_state(unit)
        .context("calling GetUnitFileState")?;
    Ok(state.to_string())
} //}}}
