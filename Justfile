default:
  just --choose

build:
  pnpm install
  pnpm tauri icon ./app-icon.png
  pnpm tauri build

build-docker:
  docker buildx build -t jrop/systemd-ui-builder .
  docker run --rm -dit --name=systemd-ui jrop/systemd-ui-builder tail -f /dev/null
  docker cp systemd-ui:src-tauri/target/release/bundle/deb/systemd-ui_0.0.0_amd64.deb .
  docker cp systemd-ui:src-tauri/target/release/bundle/appimage/systemd-ui_0.0.0_amd64.AppImage .
  docker rm -f systemd-ui
