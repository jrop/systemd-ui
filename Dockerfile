FROM node:18 AS builder

RUN apt-get update && \
  apt-get upgrade -y --no-install-recommends libssl-dev libglib2.0 libgtk-3-dev libjavascriptcoregtk-4.0-dev libsoup2.4-dev libwebkit2gtk-4.0-dev && \
  apt clean
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh /dev/stdin -y --default-toolchain=1.63.0
RUN ln -s /root/.cargo/bin/cargo /usr/local/bin/cargo
RUN ln -s /root/.cargo/bin/rustc /usr/local/bin/rustc
RUN npm i -g pnpm

FROM builder AS dist_builder
COPY . .
RUN pnpm i
RUN --mount=type=cache,target=/root/.cargo/git/ \
    --mount=type=cache,target=/root/.cargo/index/ \
    --mount=type=cache,target=/root/.cargo/registry/ \
    --mount=type=cache,target=./src-tauri/target/ \
    --mount=type=cache,target=./node_modules/.vite/ \
    pnpm tauri build

