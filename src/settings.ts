export type Settings = {
  terminal: "gnome-terminal" | "wezterm" | "kitty";
};

export const INITIAL_SETTINGS = {
  terminal: "gnome-terminal",
} as Settings;
