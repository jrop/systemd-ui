import { invoke as _invoke, InvokeArgs } from "@tauri-apps/api/tauri";

export type ResponseError = { error: string };
export type Response<T> = T | ResponseError;

export function isResponseError<T>(r: Response<T>): r is ResponseError {
  return typeof (r as any)?.error !== "undefined";
}

export async function invoke<T>(name: string, params?: InvokeArgs): Promise<T> {
  // console.info("invoke:", { name, params });
  const rawResponse: string = await _invoke(name, params);
  // console.info(" =>", { rawResponse });
  const response = JSON.parse(rawResponse) as Response<T>;
  if (isResponseError(response)) {
    console.error("Tauri.invoke error:", { rawResponse, response });
    throw new Error(response.error);
  }
  return response;
}
