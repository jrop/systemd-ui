import React from "react";
import { Table, Icon, Button } from "semantic-ui-react";
import { useAtom } from "jotai";
import _ from "lodash";
import { SETTINGS_ATOM } from "./atoms";
import { invoke } from "./invoke";
import { Unit as ListUnits_Unit } from "./types";

function Services(props: { type: 'session' | 'system', units: ListUnits_Unit[]; filter: string }) {
  const [settings] = useAtom(SETTINGS_ATOM);

  // compute filtered units whenever filter/units change
  const filteredUnits = React.useMemo(() => {
    return props.units.filter((u) => {
      const f = props.filter.trim();
      if (f.length < 3) return true;
      return u.name.includes(f);
    });
  }, [props.units, props.filter]);

  const [currentPage, setCurrentPage] = React.useState(1);

  const numPerPage = 50;
  const numPages = Math.ceil(filteredUnits.length / numPerPage);
  const filteredUnitsChunk =
    _.chunk(filteredUnits, numPerPage)[currentPage - 1] ?? [];

  // if filteredUnits.length changes, reset the current page to 1
  React.useEffect(() => {
    setCurrentPage(1);
  }, [filteredUnits.length]);

  const onPageClicked = React.useCallback(
    //{{{
    (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, p: number) => {
      e.preventDefault();
      e.stopPropagation();
      setCurrentPage(p);
      return false;
    },
    [], //}}}
  );

  const onUnitStart = React.useCallback(async (unit: string) => {
    //{{{
    const response = await invoke<string>("systemd_start_unit", {
      unit,
      mode: "replace",
      sessionType: props.type,
    });
    console.log("onUnitStart:", { response });
  }, [props.type]); //}}}

  const onUnitStop = React.useCallback(async (unit: string) => {
    //{{{
    const response = await invoke<string>("systemd_stop_unit", {
      unit,
      mode: "replace",
      sessionType: props.type,
    });
    console.log("onUnitStop:", { response });
  }, [props.type]); //}}}

  const onUnitOpenLogs = React.useCallback(
    async (unit: string) => {
      //{{{
      await invoke("systemd_open_unit_logs", {
        unit,
        terminal: settings.terminal,
        sessionType: props.type,
      });
    },
    [settings, props.type],
  ); //}}}

  return (
    //{{{
    <div>
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Load State</Table.HeaderCell>
            <Table.HeaderCell>Active State</Table.HeaderCell>
            <Table.HeaderCell>Sub State</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {filteredUnitsChunk.map((unit) => (
            <Table.Row key={unit.object_path}>
              <Table.Cell>{unit.name}</Table.Cell>
              <Table.Cell>{unit.load_state}</Table.Cell>
              <Table.Cell>{unit.active_state}</Table.Cell>
              <Table.Cell>{unit.sub_state}</Table.Cell>
              <Table.Cell>
                {unit.active_state !== "active" && (
                  <Button
                    icon
                    size="mini"
                    circular
                    onClick={() => onUnitStart(unit.name)}
                  >
                    <Icon name="play" />
                  </Button>
                )}
                {unit.active_state !== "inactive" && (
                  <Button
                    icon
                    size="mini"
                    circular
                    onClick={() => onUnitStop(unit.name)}
                  >
                    <Icon name="stop" />
                  </Button>
                )}
                {/*
                {unit.active_state === "active" && (
                  <Button icon size="mini" circular>
                    <Icon name="redo" />
                  </Button>
                )}
                */}
                <Button
                  icon
                  size="mini"
                  circular
                  onClick={() => onUnitOpenLogs(unit.name)}
                >
                  <Icon name="terminal" />
                </Button>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>

      {numPages > 1 && (
        <div className="pages">
          {Array.from({ length: numPages }).map((_, p) => (
            <>
              {currentPage === p + 1 ? (
                <span key={p}>{p + 1}</span>
              ) : (
                <a href="#" key={p} onClick={(e) => onPageClicked(e, p + 1)}>
                  {p + 1}
                </a>
              )}{" "}
            </>
          ))}
        </div>
      )}
    </div>
  ); //}}}
}

export default Services;
