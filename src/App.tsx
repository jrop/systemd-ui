import { usePopupState } from "@jrop/hooks/lib/usePopupState";
import { useAtom } from "jotai";
import React from "react";
import { Icon, Button, Input, Modal, Dropdown, Tab } from "semantic-ui-react";

import { SETTINGS_ATOM } from "./atoms";
import { invoke } from "./invoke";
import { Settings } from "./settings";
import Services from "./Services";
import { Unit as ListUnits_Unit } from "./types";

import _ from "lodash";

import "./App.css";

function App() {
  const mounted = React.useRef(false);

  const [settings, setSettings] = useAtom(SETTINGS_ATOM);
  const settingsPopup = usePopupState<Settings, Settings | undefined>(settings);

  const [systemUnits, setSystemUnits] = React.useState<ListUnits_Unit[]>([]);
  const [userUnits, setUserUnits] = React.useState<ListUnits_Unit[]>([]);
  const [filter, setFilter] = React.useState("");

  const refreshUnits = React.useCallback(async () => {
    //{{{
    const [systemResult, userResult] = await Promise.all([
      invoke<{ units: ListUnits_Unit[] }>("systemd_list_units", {
        sessionType: "system",
      }),
      invoke<{ units: ListUnits_Unit[] }>("systemd_list_units", {
        sessionType: "session",
      }),
    ]);

    // don't call set*() if the component is no longer mounted:
    if (!mounted.current) return;
    setSystemUnits(
      _.sortBy(
        systemResult.units.filter((unit) => /\.service$/.test(unit.name)),
        (u) => u.name.toLowerCase(),
      ),
    );
    setUserUnits(
      _.sortBy(
        userResult.units.filter((unit) => /\.service$/.test(unit.name)),
        (u) => u.name.toLowerCase(),
      ),
    );
  }, []); //}}}

  // on mount:
  React.useEffect(() => {
    //{{{
    mounted.current = true;
    refreshUnits();
    const interval = setInterval(refreshUnits, 1000);
    return () => {
      clearInterval(interval);
      mounted.current = false;
    };
  }, []); //}}}

  const onSettingsClicked = React.useCallback(async () => {
    //{{{
    const newSettings = await settingsPopup.showForResult(settings);
    if (!newSettings) return;
    setSettings(newSettings);
  }, [settingsPopup, settings]); //}}}

  return (
    //{{{
    <div>
      <div className="toolbar">
        <div className="toolbar-container">
          <h1>systemd: services</h1>
          <div className="toolbar-controls">
            <Input
              icon="filter"
              iconPosition="left"
              value={filter}
              onChange={(e) => setFilter((e.target as any).value)}
            />
            &nbsp;
            <Button icon circular onClick={() => onSettingsClicked()}>
              <Icon name="setting" />
            </Button>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <div className="container">
        <Tab
          panes={[
            {
              menuItem: "System",
              render: () => (
                <Services type="system" units={systemUnits} filter={filter} />
              ),
            },
            {
              menuItem: "User",
              render: () => (
                <Services type="session" units={userUnits} filter={filter} />
              ),
            },
          ]}
        />
      </div>

      <Modal
        open={settingsPopup.open}
        onClose={() => settingsPopup.resolve(undefined)}
      >
        <Modal.Header>Settings</Modal.Header>
        <Modal.Content>
          Terminal
          <br />
          <Dropdown
            selection
            value={settingsPopup.state?.terminal}
            onChange={(_e, data) =>
              settingsPopup.setState({ terminal: data.value as any })
            }
            options={[
              {
                key: "gnome-terminal",
                value: "gnome-terminal",
                text: "Gnome Terminal",
              },
              {
                key: "kitty",
                value: "kitty",
                text: "Kitty",
              },
              {
                key: "wezterm",
                value: "wezterm",
                text: "WezTerm",
              },
            ]}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={() => settingsPopup.resolve(undefined)}>
            Cancel
          </Button>
          <Button onClick={() => settingsPopup.resolve(settingsPopup.state)}>
            Ok
          </Button>
        </Modal.Actions>
      </Modal>
    </div>
  ); //}}}
}

export default App;
