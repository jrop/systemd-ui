import { atomWithStorage } from "jotai/utils";
import { INITIAL_SETTINGS } from "./settings";

export const SETTINGS_ATOM = atomWithStorage("settings", INITIAL_SETTINGS);
