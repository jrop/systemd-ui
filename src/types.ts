export type Unit = {
  active_state: string;
  description: string;
  follow_unit: string;
  job_id: number;
  job_path: string;
  job_type: string;
  load_state: string;
  name: string;
  object_path: string;
  sub_state: string;
};
